import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {color: {name: "Not determined yet!", hex: "#000000"}};
  }

  componentDidMount() {
    var today = new Date().getDate();
    var url = `http://${window.location.hostname}:${window.location.port}/color/${today}`;

    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ color: data }));
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />

          <h1>Today's color is:</h1>
          <h2 style={{ color: this.state.color.hex }} id="color">{this.state.color.name}</h2>
        </header>
      </div>
    );
  }
}

export default App;
